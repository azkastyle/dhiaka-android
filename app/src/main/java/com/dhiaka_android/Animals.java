package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.dhiaka_android.adapter.AdapterAnimals;
import com.dhiaka_android.config.ApiService;
import com.dhiaka_android.config.RetrofitAnimals;
import com.dhiaka_android.model.AnimalsItem;
import com.dhiaka_android.model.ResponseAnimals;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.View;
import android.widget.Toast;


public class Animals extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animals);
        setTitle("Kambing");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.rvListAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        tampilAnimals();
    }
    private void tampilAnimals() {
        ApiService api = RetrofitAnimals.getApiService();
        Call<ResponseAnimals> animalsCall = api.request_show_all_animals();
        animalsCall.enqueue(new Callback<ResponseAnimals>() {
            @Override
            public void onResponse(Call<ResponseAnimals> call, Response<ResponseAnimals> response) {
                if (response.isSuccessful()){
                    Log.d("response api", response.body().toString());
                    List<AnimalsItem> data_animals = response.body().getAnimals();
                    boolean status = response.body().isStatus();
                    if (status){
                        AdapterAnimals adapter = new AdapterAnimals(Animals.this, data_animals);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Toast.makeText(Animals.this, "Tidak ada produk untuk saat ini", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseAnimals> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}