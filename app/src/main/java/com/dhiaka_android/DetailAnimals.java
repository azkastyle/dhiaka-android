package com.dhiaka_android;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.dhiaka_android.config.Rest;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailAnimals extends AppCompatActivity {
    ImageView ivGambarAnimals, ic_wa, ic_telp;
    TextView tvTglTerbit, tvPenulis, tv_detail_harga, tv_detail_stock;
    WebView wvKontenAnimals;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_animals);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Detail Animals");

        ivGambarAnimals = (ImageView) findViewById(R.id.ivGambarAnimals);
        tvTglTerbit = (TextView) findViewById(R.id.tvTglTerbit);
        tvPenulis = (TextView) findViewById(R.id.tvPenulis);
        tv_detail_harga = (TextView) findViewById(R.id.tv_detail_harga);
        tv_detail_stock = (TextView) findViewById(R.id.tv_detail_stock);
        wvKontenAnimals = (WebView) findViewById(R.id.wvKontenAnimals);
        ic_wa = (ImageView) findViewById(R.id.ic_wa);
        final String no_hp = getIntent().getStringExtra("NOHP_PETERNAK");
        ic_wa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm=getPackageManager();
                try {
                    String toNumber = no_hp;
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + toNumber + "?body=" + ""));
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(DetailAnimals.this,"it may be you dont have whats app",Toast.LENGTH_LONG).show();

                }
            }
        });

        showDetailAnimals();
    }

    public void location(View view){
        Bundle data = getIntent().getExtras();

        double latitude = data.getDouble("LATITUDE");
        double longitude = data.getDouble("LONGITUDE");
        String penulis_posting = data.getString("PNS_POSTING");
        System.out.println("latitude1" + latitude);
        System.out.println("longitude1" + longitude);
        System.out.println("penulis"+ penulis_posting);

        Intent send = new Intent(DetailAnimals.this, Location.class);
        send.putExtra("LATITUDE", latitude);
        send.putExtra("LONGITUDE", longitude);
        send.putExtra("PENULIS", penulis_posting);
        startActivity(send);
    }
    private void showDetailAnimals() {
        Bundle data = getIntent().getExtras();
        int id = data.getInt("ID");
        String judul_posting = data.getString("JDL_POSTING");
        String tanggal_berita = data.getString("TGL_POSTING");
        String penulis_posting = data.getString("PNS_POSTING");
        String foto_animals = data.getString("FTO_ANIMALS");
        String isi_kontent = data.getString("ISI_KONTENT");
        int harga = data.getInt("HARGA_ANIMALS");
        int stock = data.getInt("STOCK_ANIMALS");
        System.out.println("harga"+ harga);
        System.out.println("stock"+ stock);
        System.out.println("judul"+ judul_posting);
        System.out.println("tanggal"+ tanggal_berita);
        System.out.println("penulis"+ penulis_posting);
        System.out.println("foto"+ foto_animals);
        System.out.println("kontent"+ isi_kontent);

        getSupportActionBar().setTitle(judul_posting);
        tvPenulis.setText(penulis_posting);
        tvTglTerbit.setText(tanggal_berita);
        tv_detail_harga.setText("Rp." + harga);
        tv_detail_stock.setText(stock + " Ekor Hewan");
        Glide.with(this)
                .load(foto_animals)
                .placeholder(R.drawable.no_image_available)
                .error(R.drawable.no_image_available)
                .into(ivGambarAnimals);
        wvKontenAnimals.getSettings().setJavaScriptEnabled(true);
        wvKontenAnimals.loadData(isi_kontent, "text/html; charset=utf-8", "UTF-8");
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return  true;
    }
}