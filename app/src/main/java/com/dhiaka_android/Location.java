package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Location extends AppCompatActivity implements OnMapReadyCallback {

    public Location() {
        // Required empty public constructor
    }

    private GoogleMap mMap;
    private SupportMapFragment mapFrag;
    private static final String TAG = "LocationMapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setTitle("Lokasi Penjual");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapViewLocation);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapViewLocation);

        mapFrag.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Bundle data = getIntent().getExtras();
        double latitude = data.getDouble("LATITUDE");
        double longitude = data.getDouble("LONGITUDE");
        String penulis_posting = data.getString("PENULIS");
        System.out.println("latitudenew" + latitude);
        System.out.println("longitudenew" + longitude);
        System.out.println("penulisnew"+ penulis_posting);
        mMap = googleMap;

        LatLng lokasiPenjual = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(lokasiPenjual).title(penulis_posting).draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_peternak)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lokasiPenjual,15));

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}