package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.dhiaka_android.config.LoginService;
import com.dhiaka_android.model.User;
import com.dhiaka_android.util.PrefUtil;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPage extends AppCompatActivity {

    Button login;

    private EditText usernameText;
    private EditText passwordText;
    private LoginService loginService;
    private CheckBox ShowPass;

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginPage.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        if(isSessionLogin()) {
            MenuUtama.start(this);
            LoginPage.this.finish();
        }
        usernameText = (EditText) findViewById(R.id.login_username);
        passwordText = (EditText) findViewById(R.id.login_password);
        ShowPass = findViewById(R.id.showPass);

        //Set onClickListener, untuk menangani kejadian saat Checkbox diklik
        ShowPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ShowPass.isChecked()){
                    //Saat Checkbox dalam keadaan Checked, maka password akan di tampilkan
                    passwordText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else {
                    //Jika tidak, maka password akan di sembuyikan
                    passwordText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        login = (Button)findViewById(R.id.login_btn_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                loginAct();
                Intent next = new Intent(LoginPage.this, MenuUtama.class);
                startActivity(next);
            }
        });
    }

    void loginAct() {
        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        if(TextUtils.isEmpty(username)) {
            usernameText.setError("Username belum diisi!");
            return;
        }

        if(TextUtils.isEmpty(password)) {
            passwordText.setError("Password belum diisi");
            return;
        }
        loginService = new LoginService(this);
        loginService.doLogin(username, password, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                User user = (User) response.body();

                if(user != null) {
                    if(!user.isError()) {
                        PrefUtil.putUser(LoginPage.this, PrefUtil.USER_SESSION, user);
                        PrefUtil.putString(LoginPage.this, "first_name", user.getData().getFirst_name());
                        PrefUtil.saveAttributesUserProfile(LoginPage.this,
                                user.getData().getId(),
                                user.getData().getUsername(),
                                user.getData().getFirst_name(),
                                user.getData().getLast_name(),
                                user.getData().getAlamat(),
                                user.getData().getEmail(),
                                user.getData().getPhone(),
                                user.getData().getFoto());
                        MenuUtama.start(LoginPage.this);
                        LoginPage.this.finish();
                    }

                    Toast.makeText(LoginPage.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(LoginPage.this, "An error occurred!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    boolean isSessionLogin() {
        return PrefUtil.getUser(this, PrefUtil.USER_SESSION) != null;
    }
    public  void register(View view){
        Intent next = new Intent(LoginPage.this, Register.class);
        startActivity(next);
    }
}