package com.dhiaka_android;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Maps extends AppCompatActivity implements OnMapReadyCallback {

    public Maps() {
        // Required empty public constructor
    }

    private GoogleMap mMap;
    private SupportMapFragment mapFrag;
    private static final String TAG = "LaundryMapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setTitle("Maps");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapViewLaundry);

        mapFrag.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng ft = new LatLng(-6.876945, 109.080334);
        mMap.addMarker(new MarkerOptions().position(ft).title("Toko Batik Dhiaka").draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.logomaps)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ft,15));

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
