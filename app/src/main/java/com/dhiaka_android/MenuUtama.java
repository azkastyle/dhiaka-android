package com.dhiaka_android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.dhiaka_android.util.PrefUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class MenuUtama extends AppCompatActivity {

    private Drawer.Result navigationDrawerLeft;
    private AccountHeader.Result headernavigationLeft;

    public static void start(Context context) {
        Intent intent = new Intent(context, MenuUtama.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        headernavigationLeft = new AccountHeader()
                .withActivity(this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
//                .withHeaderBackground(R.color.colorPrimary)
                .withHeaderBackground(R.drawable.navdrawst)
                .addProfiles(
                        new ProfileDrawerItem().withName("Dhiaka").withEmail("dhiaka@gmail.com").withIcon(getResources().getDrawable(R.drawable.users))
                )
                .build();

        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowToolbar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(headernavigationLeft)
                .withSelectedItem(0)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l, IDrawerItem drawerItem) {

                        if (drawerItem != null) {
                            Intent intent = null;
                            if (drawerItem.getIdentifier() == 1) {
                                intent = new Intent(MenuUtama.this, ProfilePage.class);
                            }else if (drawerItem.getIdentifier() == 2){


                            } else if (drawerItem.getIdentifier() == 4){

                            }
                            else if (drawerItem.getIdentifier() == 5){


                            }else if (drawerItem.getIdentifier() == 6){
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        MenuUtama.this);
                                builder.setIcon(R.mipmap.ic_launcher);
                                builder.setTitle("LOGOUT");
                                builder.setMessage("Apakah anda yakin ingin keluar aplikasi?");
                                builder.setNegativeButton("Tidak, gagalkan!",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Toast.makeText(MenuUtama.this,
                                                        "Anda tidak jadi keluar aplikasi!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                builder.setPositiveButton("Ya, LOGOUT!",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                logoutAct();
                                                LoginPage.start(MenuUtama.this);
                                                MenuUtama.this.finish();
                                            }
                                        });
                                builder.show();


                            }
                            if (intent != null) {
                                MenuUtama.this.startActivity(intent);
                            }
                        }

                        return;


                    }
                })
                .build();


        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Profil").withIdentifier(1).withIcon(getResources().getDrawable(R.drawable.profile)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Notifikasi").withIdentifier(2).withIcon(getResources().getDrawable(R.drawable.ntf)));

        navigationDrawerLeft.addItem(new SectionDrawerItem().withName("Konfigurasi"));
//        navigationDrawerLeft.addItem(new DividerDrawerItem());
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Tentang").withIdentifier(4).withIcon(getResources().getDrawable(R.drawable.aboutt)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Bantuan").withIdentifier(5).withIcon(getResources().getDrawable(R.drawable.info)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Keluar").withIdentifier(6).withIcon(getResources().getDrawable(R.drawable.signout)));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    public void  order(View view){
        Intent next = new Intent(MenuUtama.this, OrderPage.class);
        startActivity(next);
    }

    public void  maps(View view){
        Intent next = new Intent(MenuUtama.this, Maps.class);
        startActivity(next);
    }
    public void webViewPage(View view){
        Intent webView = new Intent(MenuUtama.this, WebViewPage.class);
        startActivity(webView);
    }
    public void videoSarung(View view){
        Intent videoSarung = new Intent(MenuUtama.this, VideoPlayer.class);
        startActivity(videoSarung);
    }
    public void vRWista(View view){
        Intent videoSarung = new Intent(MenuUtama.this, VRWisata.class);
        startActivity(videoSarung);
    }

    public void Animals(View view){
        Intent videoSarung = new Intent(MenuUtama.this, Animals.class);
        startActivity(videoSarung);
    }

    void logoutAct() {
        PrefUtil.clear(this);
    }
}