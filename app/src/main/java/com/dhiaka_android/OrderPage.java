package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class OrderPage extends AppCompatActivity {
    int harga, quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_page);
        harga = 25000;
    }
    public void increment(View view){
        quantity = quantity+1 ;
        display(quantity);
        displayTotal(quantity);
    }
    public void decrement(View view){
        if (quantity==1){
            Toast.makeText(this,"Jumlah Minimal 1",Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity -1;
        display(quantity);
        displayTotal(quantity);
    }

    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_textview);
        quantityTextView.setText("" + number);
    }

    private void displayTotal(int number) {
        TextView totalTextView = (TextView) findViewById(R.id.txt_total);
        totalTextView.setText("" + number * harga);
    }
}