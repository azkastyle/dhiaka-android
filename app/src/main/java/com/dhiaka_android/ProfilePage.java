package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhiaka_android.config.Rest;
import com.dhiaka_android.model.User;
import com.dhiaka_android.util.PrefUtil;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilePage extends AppCompatActivity {

    public CircleImageView foto;
    public TextView username, nama_lengkap, email, alamat, phone;

    ImageView edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        setTitle("Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        foto = (CircleImageView)findViewById(R.id.foto);
        username = (TextView)findViewById(R.id.userName);
        nama_lengkap = (TextView)findViewById(R.id.namaLengkap);
        email = (TextView)findViewById(R.id.email);
        alamat = (TextView)findViewById(R.id.alamat);
        phone = (TextView)findViewById(R.id.nohp);
        edit = (ImageView)findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent next = new Intent(ProfilePage.this, UpdateProfile.class);
//                startActivity(next);
            }
        });

        User user = PrefUtil.getUser(this, PrefUtil.USER_SESSION);

        String datafirst_name = PrefUtil.getString(this, "first_name");
        String datalast_name = PrefUtil.getString(this, "last_name");
        String datausername = PrefUtil.getString(this, "username");
        String dataemail = PrefUtil.getString(this, "email");
        String dataalamat = PrefUtil.getString(this, "alamat");
        String dataphone = PrefUtil.getString(this, "phone");
        String datafoto = PrefUtil.getString(this, "foto");

        username.setText(datausername);
        nama_lengkap.setText(datafirst_name + " " + datalast_name);
        alamat.setText(dataalamat);
        email.setText(dataemail);
        phone.setText(dataphone);

        final String urlFotoProfile = Rest.GAMBAR + datafoto;
        // Set image ke widget dengna menggunakan Library Piccasso
        // krena imagenya dari internet
        Glide.with(this)
                .load(urlFotoProfile)
                .placeholder(R.drawable.no_image_available)
                .error(R.drawable.no_image_available)
                .into(foto);
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return  true;
    }
}