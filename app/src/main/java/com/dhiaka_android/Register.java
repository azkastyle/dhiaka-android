package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dhiaka_android.config.RegisterService;
import com.dhiaka_android.model.BaseResponse;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    EditText edt_username, edt_first_name, edt_last_name, edt_alamat, edt_email, edt_phone, edt_password, edt_cpass;
    Button btn_register;
    private RegisterService registerService;

    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        edt_username = findViewById(R.id.regis_username);
        edt_first_name = findViewById(R.id.regis_name_first);
        edt_last_name = findViewById(R.id.regis_name_last);
        edt_alamat = findViewById(R.id.regis_alamat);
        edt_email = findViewById(R.id.regis_email);
        edt_phone = findViewById(R.id.regis_nohp);
        edt_password = findViewById(R.id.regis_password);
        edt_cpass = findViewById(R.id.confirm_password);

        btn_register = findViewById(R.id.regis_btn_regis);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerAct();
            }
        });
    }
    public  void login(View view){
        Intent next = new Intent(Register.this, LoginPage.class);
        startActivity(next);
    }
    void registerAct() {
        String username = edt_username.getText().toString().trim();
        String first_name = edt_first_name.getText().toString();
        String last_name = edt_last_name.getText().toString();
        String alamat = edt_alamat.getText().toString();
        String email = edt_email.getText().toString();
        String phone = edt_phone.getText().toString().trim();
        String password = edt_password.getText().toString();
        String cPass = edt_cpass.getText().toString();

        if (TextUtils.isEmpty(username)) {
            edt_username.setError("Username harus diisi !");
            return;
        }
        if(username.trim().length() < 3 ) {
            edt_username.setError("Username Minimal 3");
            return;
        }
        if (TextUtils.isEmpty(first_name)) {
            edt_first_name.setError("Nama Depan harus diisi !");
            return;
        }
        if (TextUtils.isEmpty(last_name)) {
            edt_last_name.setError("Nama Belakang harus diisi !");
            return;
        }
        if (TextUtils.isEmpty(alamat)) {
            edt_alamat.setError("Alamat harus diisi !");
            return;
        }
        if (TextUtils.isEmpty(email)) {
            edt_email.setError("Email harus diisi !");
            return;
        }
        if(!EMAIL_ADDRESS_PATTERN.matcher(email).matches()){
            edt_email.setError("Email Tidak Valid");
            return;
        }
        if (TextUtils.isEmpty(phone)) {
            edt_phone.setError("No Handphone harus diisi !");
            return;
        }
        if(phone.trim().length() < 10  ) {
            edt_phone.setError("No Handphone Minimal 10");
            return;
        }
        if(phone.trim().length() > 13 ) {
            edt_phone.setError("No Handphone Maksimal 13");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            edt_password.setError("Password harus diisi !");
            return;
        }
        if (TextUtils.isEmpty(cPass)) {
            edt_cpass.setError("Confirm Password harus diisi !");
            return;
        }
        if(!password.equals(cPass)){
            edt_cpass.setError("Password Tidak Sama !");
            return;
        }
        registerService = new RegisterService(this);
        registerService.doRegister(username, first_name, last_name, alamat, email, phone, password, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                BaseResponse baseResponse = (BaseResponse) response.body();
                if (baseResponse != null) {
                    if (!baseResponse.isError()) {
                        LoginPage.start(Register.this);
                        Register.this.finish();
                    }
                    Toast.makeText(Register.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Toast.makeText(Register.this, "An error occured!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}