package com.dhiaka_android;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.VideoView;
import java.net.URL;

public class VideoPlayer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        VideoView video = (VideoView)findViewById(R.id.video);
        Uri letakVideo = Uri.parse("android.resource://"+ getPackageName()+"/"+R.raw.video_sarung);
        video.setVideoURI(letakVideo);
        video.start();
    }
}