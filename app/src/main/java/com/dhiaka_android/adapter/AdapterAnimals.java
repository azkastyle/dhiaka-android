package com.dhiaka_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dhiaka_android.DetailAnimals;
import com.dhiaka_android.R;
import com.dhiaka_android.model.AnimalsItem;
import com.dhiaka_android.config.Rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterAnimals extends RecyclerView.Adapter<AdapterAnimals.MyViewHolder> {
    Context context;
    List<AnimalsItem> animals;
    public AdapterAnimals(Context context, List<AnimalsItem> data_animals) {
        // Inisialisasi
        this.context = context;
        this.animals = data_animals;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.animals_item, parent, false);

        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String string_date = animals.get(position).getTanggalPosting();

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = f.parse(string_date);
            long milliseconds = d.getTime();
            holder.tvTglPosting.setText(Rest.getTimeAgo(milliseconds));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvJudul.setText(animals.get(position).getJudulPosting());
        holder.tvPenulis.setText(animals.get(position).getPenulis());
        // Dapatkan url gambar
        final String urlGambarAnimals = Rest.GAMBAR + animals.get(position).getFoto();
        // Set image ke widget dengna menggunakan Library Piccasso
        // krena imagenya dari internet
        Glide.with(context)
                .load(urlGambarAnimals)
                .placeholder(R.drawable.no_image_available)
                .error(R.drawable.no_image_available)
                .into(holder.ivGambarAnimals);

        // Event klik ketika item list nya di klik
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mulai activity Detail
                Intent varIntent = new Intent(context, DetailAnimals.class);
                Bundle data = new Bundle();
                data.putString("JDL_POSTING", animals.get(position).getJudulPosting());
                String string_date = animals.get(position).getTanggalPosting();
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date d = f.parse(string_date);
                    long milliseconds = d.getTime();
                    data.putString("TGL_POSTING", Rest.getTimeAgo(milliseconds));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                data.putInt("ID", animals.get(position).getId());
                data.putString("PNS_POSTING", animals.get(position).getPenulis());
                data.putString("FTO_ANIMALS", urlGambarAnimals);
                data.putInt("HARGA_ANIMALS", animals.get(position).getHarga());
                data.putInt("STOCK_ANIMALS", animals.get(position).getStock());
                data.putString("NOHP_PETERNAK", animals.get(position).getNohp());
                data.putString("ISI_KONTENT", animals.get(position).getIsiKontent());
                data.putDouble("LATITUDE", animals.get(position).getLatitude());
                data.putDouble("LONGITUDE", animals.get(position).getLongitude());
                varIntent.putExtras(data);
                context.startActivity(varIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivGambarAnimals;
        TextView tvJudul, tvTglPosting, tvPenulis;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivGambarAnimals = (ImageView) itemView.findViewById(R.id.ivPosterAnimals);
            tvJudul = (TextView) itemView.findViewById(R.id.tvJudulPosting);
            tvTglPosting = (TextView) itemView.findViewById(R.id.tvTglPosting);
            tvPenulis = (TextView) itemView.findViewById(R.id.tvPenulis);
        }
    }
}
