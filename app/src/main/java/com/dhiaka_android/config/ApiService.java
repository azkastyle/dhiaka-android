package com.dhiaka_android.config;

import com.dhiaka_android.model.ResponseAnimals;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("animals.json")
    Call<ResponseAnimals> request_show_all_animals();
//    @GET("databerita")
//    Call<ResponseAnimals> request_show_all_animals();
}

