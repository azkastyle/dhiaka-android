package com.dhiaka_android.config;

import com.dhiaka_android.model.User;

import retrofit2.http.FormUrlEncoded;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginInterface {

    @FormUrlEncoded
    @POST(Rest.API_LOGIN)
    Call<User> login(
            @Field("username") String username,
            @Field("password") String password);

}