package com.dhiaka_android.config;

import android.content.Context;

import retrofit2.Callback;
public class LoginService {

    private LoginInterface loginInterface;

    public LoginService(Context context) {
        loginInterface = RetrofitBuilder.builder(context)
                .create(LoginInterface.class);
    }
    public void doLogin(String username, String password, Callback callback) {
        loginInterface.login(username, password).enqueue(callback);
    }
}

