package com.dhiaka_android.config;

import com.dhiaka_android.model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Field;

public interface RegisterInterface {

    @FormUrlEncoded
    @POST(Rest.API_REGISTER)
    Call<BaseResponse> register(
            @Field("username") String username,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("password") String password);
}

