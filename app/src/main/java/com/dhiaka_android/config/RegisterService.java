package com.dhiaka_android.config;

import android.content.Context;

import retrofit2.Callback;

public class RegisterService {

    private RegisterInterface registerInterface;

    public RegisterService(Context context) {
        registerInterface = RetrofitBuilder.builder(context)
                .create(RegisterInterface.class);
    }

    public void doRegister(String username, String first_name, String last_name, String alamat, String email, String phone, String password, Callback callback) {
        registerInterface.register(username, first_name, last_name, alamat, email, phone, password).enqueue(callback);
    }

}
