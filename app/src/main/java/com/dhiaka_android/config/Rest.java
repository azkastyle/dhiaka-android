package com.dhiaka_android.config;

import java.util.Calendar;

public class Rest {
    public static final String DOMAIN = "http://192.168.43.112/Restfull-API-Dhiaka/";
    public static final String API_LOGIN = DOMAIN + "api/auth/login";
    public static final String API_REGISTER = DOMAIN + "api/auth/register";
    public static final  String GAMBAR = "https://gitlab.com/azkastyle/gofarms-json/raw/master/";
//    public static final  String GAMBAR = DOMAIN + "uploads/";
    public static final  String API_BERITA = "https://gitlab.com/azkastyle/gofarms-json/raw/master/";

    public static final String[] Months = {
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "Nopember",
            "Desember"
    };

    public static String getTimeAgo(long time) {
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        if (time < 1000000000000L) {
            time *= 1000;
        }
        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "baru saja";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "satu menit yang lalu";
        } else if (diff < 60 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " menit yang lalu";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " jam yang lalu";
        } else if (diff < 2 * 24 * HOUR_MILLIS) {
            return "kemarin";
        } else if (diff < 7 * 24 * HOUR_MILLIS) {
            return diff / DAY_MILLIS + " hari yang lalu";
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            return calendar.get(Calendar.DAY_OF_MONTH) + " " + Months[calendar.get(Calendar.MONTH)] + " " + calendar.get(Calendar.YEAR);
        }
    }
}


