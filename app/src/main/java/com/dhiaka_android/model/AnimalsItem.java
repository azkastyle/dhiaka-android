package com.dhiaka_android.model;

import com.google.gson.annotations.SerializedName;

public class AnimalsItem{

    @SerializedName("penulis")
    private String penulis;

    @SerializedName("foto")
    private String foto;

    @SerializedName("id")
    private int id;

    @SerializedName("judul_posting")
    private String judulPosting;

    @SerializedName("tanggal_posting")
    private String tanggalPosting;

    @SerializedName("harga")
    private int harga;

    @SerializedName("stock")
    private int stock;

    @SerializedName("nohp")
    private String nohp;

    @SerializedName("isi_content")
    private String isiKontent;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    public void setPenulis(String penulis){
        this.penulis = penulis;
    }

    public String getPenulis(){
        return penulis;
    }

    public void  setHarga(Integer harga){this.harga = harga;}

    public int getHarga(){return harga;}

    public void  setStock(Integer stock){this.stock = stock;}

    public int getStock(){return stock;}

    public void setNohp(String nohp){this.nohp = nohp;}

    public String getNohp(){return nohp;}

    public void setFoto(String foto){
        this.foto = foto;
    }

    public String getFoto(){
        return foto;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setJudulPosting(String judulPosting){
        this.judulPosting = judulPosting;
    }

    public String getJudulPosting(){
        return judulPosting;
    }

    public void setTanggalPosting(String tanggalPosting){
        this.tanggalPosting = tanggalPosting;
    }

    public String getTanggalPosting(){
        return tanggalPosting;
    }

    public void setIsiKontent(String isiBerita){
        this.isiKontent = isiKontent;
    }

    public String getIsiKontent(){
        return isiKontent;
    }

    public double getLatitude(){return latitude;}

    public void  setLatitude(Integer latitude){this.latitude = latitude;}

    public double getLongitude(){return longitude;}

    public void  setLongitude(Integer longitude){this.longitude = longitude;}

    @Override
    public String toString(){
        return
                "AnimalsItem{" +
                        "penulis = '" + penulis + '\'' +
                        ",foto = '" + foto + '\'' +
                        ",id = '" + id + '\'' +
                        ",judul_posting = '" + judulPosting + '\'' +
                        ",tanggal_posting = '" + tanggalPosting + '\'' +
                        ",isi_content = '" + isiKontent + '\'' +
                        "}";
    }
}

