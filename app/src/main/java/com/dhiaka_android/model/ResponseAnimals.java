package com.dhiaka_android.model;
;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseAnimals{

    @SerializedName("animals")
    private List<AnimalsItem> animals;

    @SerializedName("status")
    private boolean status;

    public void setBerita(List<AnimalsItem> animals){
        this.animals = animals;
    }

    public List<AnimalsItem> getAnimals(){
        return animals;
    }

    public void setStatus(boolean status){
        this.status = status;
    }

    public boolean isStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "ResponseAnimals{" +
                        "animals = '" + animals + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}
