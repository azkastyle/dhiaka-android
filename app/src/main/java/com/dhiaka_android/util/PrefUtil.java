package com.dhiaka_android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.dhiaka_android.model.User;
import com.dhiaka_android.model.UserData;

public class PrefUtil {

    public static final String USER_SESSION = "user_session";

    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void putUser(Context context, String key, User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        putString(context, key, json);
    }
    public static User getUser(Context context, String key) {
        Gson gson = new Gson();
        String json = getString(context, key);
        User user = gson.fromJson(json, User.class);
        return user;
    }

    public static void saveAttributesUserProfile(Context context, String Id, String Username, String FirstName,
                                                 String LastName, String Alamat, String Email, String Phone, String Photo) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString("id", Id);
        editor.putString("username", Username);
        editor.putString("first_name", FirstName);
        editor.putString("last_name", LastName);
        editor.putString("alamat", Alamat);
        editor.putString("email", Email);
        editor.putString("phone", Phone);
        editor.putString("foto", Photo);
        editor.apply();
    }

    public static void saveAttributesUserProfileData(Context context, String Id, String Username, String FirstName,
                                                     String LastName, String Alamat, String Email, String Phone) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString("id", Id);
        editor.putString("username", Username);
        editor.putString("first_name", FirstName);
        editor.putString("last_name", LastName);
        editor.putString("alamat", Alamat);
        editor.putString("email", Email);
        editor.putString("phone", Phone);
        editor.apply();
    }

    public static void putString(Context context, String key, String value) {
        getSharedPreference(context).edit().putString(key, value).apply();
    }

    public static String getString(Context context, String key) {
        return getSharedPreference(context).getString(key, null);
    }

    public static void clear(Context context) {
        getSharedPreference(context).edit().clear().apply();
    }

}